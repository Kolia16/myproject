/*--------------Скроли навігація по сторінці---------------*/
var scrollOnSuggestions = document.getElementById('scroll-on-suggestions');

scrollOnSuggestions.addEventListener("click", function() {
    window.scrollTo(0, 660);
})

var scrollOnNewAuto = document.getElementById('scroll-on-new-auto');

scrollOnNewAuto.addEventListener("click", function() {
    window.scrollTo(0, 1540);
})

var scrollOurServices = document.getElementById('scroll-our-services');

scrollOurServices.addEventListener("click", function() {
    window.scrollTo(0, 2000);
})

/*----------------------Головний слайдер------------------------*/

var slider = new Array(
    "imges/main-slider/чёрная-машина.jpg",
    "imges/main-slider/ford-mustang-wallpapers-hd-71552-299522.png",
    "imges/main-slider/car-wallpapers-black-audi-s8.jpg",
);


var leftButtonSlidr = document.querySelector('.left-button');
leftButtonSlidr.addEventListener("click", function(){
    resultSlider.minus();
});

var rightButtonSlidr = document.querySelector('.right-button');
rightButtonSlidr.addEventListener("click", function(){
    resultSlider.plus();
});

function imgSlider(slider){
    var count = 0;
    var arrImges = slider;
    
    this.get = function(){
        console.log(count);
        console.log(arrImges);
    }
    this.plus = function(){
        count++;
        checkConditions();
    }
    
    this.minus = function(){
        count--;
        checkConditions();
    }
    
    function checkConditions(){
        if(count<0){
            count = arrImges.length - 1;
            checkConditions();
        }else if(count>(arrImges.length-1)){
            count = 0;
            checkConditions();
        }else{
            var elem = document.querySelector('.slider');
            var backgroundImage ="background-image: url(\""+arrImges[count]+"\");";
            elem.style.cssText = backgroundImage;
        }   
    }
}

var resultSlider = new imgSlider(slider);

var timerId = setTimeout(function tick() {
  resultSlider.plus();
  timerId = setTimeout(tick, 10000);
}, 1);


/*----------------------Заміна головного зображення--------------------------*/


var elem = document.querySelector('.number-imgs');

elem.onclick = function(elent){
    if(elent.target.tagName == 'IMG'){
        var mainImg = document.querySelector('.main-img > figure > img');
        mainImg.setAttribute('src',elent.target.getAttribute('src'));
    }
}

/*---------------------Представлення головного зображенн----------------------------*/

var elemImg = document.querySelector('.main-img > figure img');

elemImg.addEventListener("click", function(){
    var elemImg = document.querySelector('.main-img > figure > img');
    
    var windowsWithImg = document.createElement('div');
    windowsWithImg.className = 'windows-with-img';
    document.body.insertBefore(windowsWithImg, document.body.lastChild);

    var windowsWrapper = document.createElement('div')
    windowsWithImg.insertBefore(windowsWrapper, windowsWithImg.firstChild);

    var windowsWithImgOption = document.createElement('img')
    windowsWrapper.insertBefore(windowsWithImgOption, windowsWrapper.firstChild);
    windowsWithImgOption.setAttribute('src',elemImg.getAttribute('src'));

    var windowsButton = document.createElement('div')
    windowsWrapper.insertBefore(windowsButton, windowsWrapper.lastChild);
    windowsButton.className = 'close-button';
    
    var closeButton = document.querySelector('.close-button');
    closeButton.addEventListener("click", function(){
        document.body.removeChild(document.querySelector('.windows-with-img'));
    });
});


/*--------------------------Слайдер товару-------------------------------*/

var arrProductes = new Array(
    {img:'imges/slider/345x405-towidth-100-media_djcatalog2_images_item_8_volkswagen-jetta-3.jpg',name:'Ford Escape',described:'Eco boost мотор - экономичный и при этом динамичный. Оригинальный'},
    
    {img:'imges/slider/345x405-towidth-100-media_djcatalog2_images_item_10_land-rover-range-rover-sport-1-2-1-1.jpg',name:'Volkswagen Passat B7',described:'Бежевая кожа. Кондиционер. Круиз-контроль. Подогрев сидений.'},
    
    {img:'imges/slider/345x405-towidth-100-media_djcatalog2_images_item_10_volkswagen-passat-b7-1.jpg',name:'Audi A3',described:'Америка. Максимальная комплектация. Черная Кожа. В отличном'},
    
    {img:'imges/slider/345x405-towidth-100-media_djcatalog2_images_item_11_audi-a3-36.jpg',name:'Ford Fusion RESTYL',described:'Американец. Свежепривезенный Обслуженный. В отличном состояние'},
    
    {img:'imges/slider/345x405-towidth-100-media_djcatalog2_images_item_11_ford-fusion-restyl-6-5.jpg',name:'Audi A8 LONG',described:'Не крашена. Куплена у официального дилера. Оригинальный пробег'},
    
    {img:'imges/slider/345x405-towidth-100-media_djcatalog2_images_item_12_audi-a8-long-6-9.jpg',name:'Ford Fusion AWD',described:'Свжепривезенный. Полный привод. Максимальная комплектация'},
    
    {img:'imges/slider/345x405-towidth-100-media_djcatalog2_images_item_12_ford-fusion-awd-6.jpg',name:'Ford Mustang PERFOMANCE',described:'Лимитированная версия PERFOMANCE pkg. Данный пакет стоил по заводу'},
    
    {img:'imges/slider/345x405-towidth-100-media_djcatalog2_images_item_12_ford-mustang-perfomance-9-6.jpg',name:'Volkswagen Jetta',described:'Не крашена. Куплена у официального дилера. Сервисная книга'},
);


var backButtonSlidr = document.querySelector('.backButtonSlidr');
backButtonSlidr.addEventListener("click", function(){
    sliderNewAuto.back();
});

var nextButtonSlidr = document.querySelector('.nextButtonSlidr');
nextButtonSlidr.addEventListener("click", function(){
    sliderNewAuto.next();
});

function FunctionNewSliderAauto (arr){
    var count = 0;
    var arrProductes = arr;
    var newSliderAauto = document.querySelector('.slider-auto');
    
    this.get = function(){
        console.log(arrProductes);
        console.log(newSliderAauto);
        console.log(arrProductes.length);
    }
    
    this.next = function(){
        count++;
        checkConditions();
    }
    
    this.back = function(){
        count--;
        checkConditions();
    }
    
    function checkConditions(){
        if(count<0){
            count = arrProductes.length - 1;
            checkConditions();
        }else if(count>(arrProductes.length-1)){
            count = 0;
            checkConditions();
        }else{
            var elem = document.querySelector('.slider-auto');
            var createNewElem = document.createElement('div');
            createNewElem.innerHTML = '<img src="'+arrProductes[count].img+'"><h6>'+arrProductes[count].name+'</h6><p>'+arrProductes[count].described+'.</p><a href="#">Подробнее</a>'
            elem.appendChild(createNewElem);
            getNumberImges(); 
            if(getNumberImges()>4){
                elem.removeChild(elem.children[0]);
            }
        }   
    }
    function getNumberImges(){
        return (document.querySelectorAll('.slider-auto > div').length);
    }
}

var sliderNewAuto = new FunctionNewSliderAauto(arrProductes);
for(var i = 0; i < 4; i++){
    sliderNewAuto.next();
}



