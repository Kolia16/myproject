
/*-------------------Представлення головного зобрадення--------------------*/

var elemImg = document.querySelector('.main-photo > img');

elemImg.addEventListener("click", function(){
    var elemImg = document.querySelector('.main-photo > img');
    
    var windowsWithImg = document.createElement('div');
    windowsWithImg.className = 'windows-with-img';
    document.body.insertBefore(windowsWithImg, document.body.lastChild);

    var windowsWrapper = document.createElement('div')
    windowsWithImg.insertBefore(windowsWrapper, windowsWithImg.firstChild);

    var windowsWithImgOption = document.createElement('img')
    windowsWrapper.insertBefore(windowsWithImgOption, windowsWrapper.firstChild);
    windowsWithImgOption.setAttribute('src',elemImg.getAttribute('src'));

    var windowsButton = document.createElement('div')
    windowsWrapper.insertBefore(windowsButton, windowsWrapper.lastChild);
    windowsButton.className = 'close-button';
    
    var closeButton = document.querySelector('.close-button');
    closeButton.addEventListener("click", function(){
        document.body.removeChild(document.querySelector('.windows-with-img'));
    });
});

/*----------------------Заміна головного зображення--------------------------*/

var elem = document.querySelector('.list-photo>div');

elem.onclick = function(elent){
    if(elent.target.tagName == 'DIV'){
        var mainImg = document.querySelector('.main-photo > img');
        var str = getComputedStyle(elent.target).backgroundImage;
        var reg = new RegExp("imges/product/.+?JPG","ig");
        mainImg.setAttribute('src',str.match(reg));
    }
}
